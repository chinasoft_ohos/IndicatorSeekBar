/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.slice;

import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;

import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.BaseAbilitySlice;
import com.warkiz.indicatorseekbar.donation.BaseComponent;
import com.warkiz.indicatorseekbar.fragment.ContinuousComponent;
import com.warkiz.indicatorseekbar.fragment.CustomComponent;
import com.warkiz.indicatorseekbar.fragment.DiscreteComponent;
import com.warkiz.indicatorseekbar.fragment.DonationComponent;
import com.warkiz.indicatorseekbar.fragment.IndicatorComponent;
import com.warkiz.indicatorseekbar.fragment.JavaBuildComponent;
import com.warkiz.indicatorseekbar.provider.MainPagerProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 主界面
 *
 * @since 2021-04-12
 */
public class MainAbilitySlice extends BaseAbilitySlice
        implements TabList.TabSelectedListener, PageSlider.PageChangedListener {
    private String[] sType = new String[]{"continuous", "discrete", "custom", "java", "indicator", "donation"};
    private TabList tabList;
    private PageSlider pageSlider;

    private List<BaseComponent> mComponentList = new ArrayList<>();

    @Override
    public int getLayoutResId() {
        return ResourceTable.Layout_ability_main;
    }

    @Override
    protected void initCreate() {
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorPrimaryDark));
        initFragment();
        initViews();
    }

    private void initFragment() {
        mComponentList.add(new ContinuousComponent(this));
        mComponentList.add(new DiscreteComponent(this));
        mComponentList.add(new CustomComponent(this));
        mComponentList.add(new JavaBuildComponent(this));
        mComponentList.add(new IndicatorComponent(this));
        mComponentList.add(new DonationComponent(this));
    }

    private void initViews() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tabList);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pagerSlider);
        pageSlider.setSlidingPossible(true);
        tabList.addTabSelectedListener(this);
        pageSlider.addPageChangedListener(this);

        for (String title : sType) {
            TabList.Tab tab = tabList.new Tab(getContext());
            tab.setText(title.toUpperCase(Locale.ROOT));
            tabList.addTab(tab);
        }
        tabList.selectTabAt(0);

        pageSlider.setProvider(new MainPagerProvider(mComponentList));
        pageSlider.setCurrentPage(0);
        pageSlider.setReboundEffect(false);
        pageSlider.setCentralScrollMode(true);
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
    }

    @Override
    public void onPageChosen(int i) {
        if (tabList.getSelectedTab().getPosition() != i) {
            tabList.selectTab(tabList.getTabAt(i));
            if (getTabX(i) < tabList.getEstimatedWidth() / 2) {
                tabList.scrollTo(0, 0);
            } else {
                tabList.scrollTo(getTabX(i) - tabList.getEstimatedWidth() / 2, 0);
            }
        }
        mComponentList.get(i).invalidate();
    }

    @Override
    public void onSelected(TabList.Tab tab) {
        if (pageSlider.getCurrentPage() != tab.getPosition()) {
            pageSlider.setCurrentPage(tab.getPosition());
        }
    }

    @Override
    public void onUnselected(TabList.Tab tab) {
    }

    @Override
    public void onReselected(TabList.Tab tab) {
    }

    private int getTabX(int position) {
        int tabX = 0;
        if (position + 1 > tabList.getTabCount()) {
            return tabX;
        }
        for (int i = 0; i < position; i++) {
            tabX = tabX + tabList.getTabAt(i).getEstimatedWidth() + (tabList.getTabMargin() * 2);
        }
        TabList.Tab tab = tabList.getTabAt(position);
        tabX = tabX + tab.getEstimatedWidth() / 2 + tabList.getTabMargin();
        return tabX;
    }
}
