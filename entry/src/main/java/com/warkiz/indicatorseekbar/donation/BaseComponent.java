/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.donation;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ScrollView;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import com.warkiz.indicatorseekbar.ResourceTable;

/**
 * Component基类
 *
 * @since 2021-04-12
 */
public abstract class BaseComponent {
    private Context mContext;

    /**
     * BaseComponent
     *
     * @param context 上下文
     */
    public BaseComponent(Context context) {
        mContext = context;
    }

    /**
     * 创建Component
     *
     * @return 返回值
     */
    public Component create() {
        Component component = LayoutScatter.getInstance(mContext).parse(getLayoutId(), null, false);
        component.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT));
        ComponentContainer rootLayout = (ComponentContainer) component;
        Component textView = rootLayout.findComponentById(ResourceTable.Id_include_layout);
        if (textView != null) {
            textView.setClickedListener(component1 -> {
                Intent intents = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withUri(Uri.parse(getContext().getString(ResourceTable.String_url_github)))
                        .withAction(IntentConstants.ACTION_SEARCH)
                        .build();
                intents.setOperation(operation);
                mContext.startAbility(intents, 0);
            });
        }
        ScrollView scrollView = (ScrollView) rootLayout.findComponentById(ResourceTable.Id_scroll);
        if (scrollView != null) {
            scrollView.enableScrollBar(Component.VERTICAL, false);
            scrollView.setScrollbarColor(scrollView.getScrollbarBackgroundColor());
            scrollView.setScrollbarBackgroundColor(Color.TRANSPARENT);
            scrollView.setTouchEventListener(new Component.TouchEventListener() {
                @Override
                public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                    switch (touchEvent.getAction()) {
                        case TouchEvent.POINT_MOVE:
                            scrollView.enableScrollBar(Component.VERTICAL, true);
                            break;
                        case TouchEvent.PRIMARY_POINT_UP:
                        case TouchEvent.TOUCH_PANEL:
                            scrollView.enableScrollBar(Component.VERTICAL, false);
                            break;
                    }
                    return true;
                }
            });
        }
        initView(component);
        return rootLayout;
    }

    /**
     * 获取布局资源id
     *
     * @return 资源id
     */
    protected abstract int getLayoutId();

    /**
     * 初始化
     *
     * @param root 父组件
     */
    protected abstract void initView(Component root);

    public Context getContext() {
        return mContext;
    }

    /**
     * 组件刷新
     */
    public void invalidate() {
    }
}
