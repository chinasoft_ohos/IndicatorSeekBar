package com.warkiz.indicatorseekbar.donation;

import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.bundle.BundleInfo;
import ohos.bundle.IBundleManager;
import ohos.rpc.RemoteException;

/**
 * the donation utils , no impact libs
 */
public class AlipayUtil {
    private static final String ALIPAY_PACKAGE_NAME = "com.eg.ohos.AlipayGphone";
    private static final String PAY_PATH = "FKX03308HBXT5XSEPAKCBB";

    private static final String INTENT_URL_FORMAT = "intent://platformapi/startapp?saId=10000007&" +
            "clientVersion=3.7.0.0718&qrcode=https%3A%2F%2Fqr.alipay.com%2F{urlCode}%3F_s" +
            "%3Dweb-other&_t=1472443966571#Intent;" +
            "scheme=alipayqr;package=com.eg.ohos.AlipayGphone;end";

    public static boolean startAlipayClient(Context context, String urlCode) {
        return startIntentUrl(context, INTENT_URL_FORMAT.replace("{urlCode}", urlCode));
    }

    public static boolean startAlipayClient(Context context) {
        return startIntentUrl(context, INTENT_URL_FORMAT.replace("{urlCode}", PAY_PATH));
    }

    public static boolean startIntentUrl(Context context, String intentFullUrl) {
        Intent intent = Intent.parseUri(intentFullUrl);
        context.startAbility(intent, 0);
        return true;
    }

    public static boolean hasInstalledAlipayClient(Context context) {
        IBundleManager bundleManager = context.getBundleManager();
        try {
            BundleInfo info = bundleManager.getBundleInfo(ALIPAY_PACKAGE_NAME, 0);
            return info != null;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }
}
