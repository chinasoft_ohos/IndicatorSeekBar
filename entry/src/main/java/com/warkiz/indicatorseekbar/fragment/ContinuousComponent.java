/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.fragment;

import com.warkiz.indicatorseekbar.utils.PixelMapUtil;
import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.BaseComponent;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.IndicatorStayLayout;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.text.Font;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

/**
 * Continuous界面
 *
 * @since 2021-04-12
 */
public class ContinuousComponent extends BaseComponent {
    private Context mContext;

    private IndicatorSeekBar indicator1;
    private IndicatorSeekBar indicator2;
    private IndicatorSeekBar indicator3;
    private IndicatorSeekBar indicator4;
    private IndicatorSeekBar indicator5;
    private IndicatorSeekBar indicator6;
    private IndicatorSeekBar indicator7;
    private IndicatorSeekBar indicator8;
    private IndicatorSeekBar indicator9;
    private IndicatorSeekBar indicator10;
    private IndicatorSeekBar indicator11;
    private IndicatorSeekBar indicator12;

    private IndicatorStayLayout stayLayout3;
    private IndicatorStayLayout stayLayout4;
    private IndicatorStayLayout stayLayout6;
    private IndicatorStayLayout stayLayout10;

    private Text textView;
    private Text states;
    private Text progress;
    private Text progressFloat;
    private Text fromUser;

    private boolean isInit = false;

    /**
     * ContinuousComponent
     *
     * @param context 上下文
     */
    public ContinuousComponent(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_continuous;
    }

    @Override
    protected void initView(Component root) {
        mContext = getContext();

        indicator1 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_1);
        indicator2 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_2);
        indicator3 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_3);
        indicator4 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_4);
        indicator5 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_5);
        indicator6 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_6);
        indicator7 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_7);
        indicator8 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_8);
        indicator9 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_9);
        indicator10 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_10);
        indicator11 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_11);
        indicator12 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_12);

        stayLayout3 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_3);
        stayLayout4 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_4);
        stayLayout6 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_6);
        stayLayout10 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_10);

        textView = (Text) root.findComponentById(ResourceTable.Id_source_code);
        states = (Text) root.findComponentById(ResourceTable.Id_states);
        progress = (Text) root.findComponentById(ResourceTable.Id_progress);
        progressFloat = (Text) root.findComponentById(ResourceTable.Id_progress_float);
        fromUser = (Text) root.findComponentById(ResourceTable.Id_from_user);
        setView();
        stayLayout3.layoutIndicator(indicator3, 0);
        stayLayout4.layoutIndicator(indicator4, 0);
        stayLayout6.layoutIndicator(indicator6, 0);
        stayLayout10.layoutIndicator(indicator10, 0);
        isInit = true;
    }

    private void setView() {
        // scale
        indicator3.setDecimalScale(4);

        // custom indicator text
        indicator4.setIndicatorTextFormat("${PROGRESS} %");

        // thumb_drawable
        indicator5.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_last_next_length_2));
        PixelMap pixelMap = PixelMapUtil.getPixelMap(mContext, ResourceTable.Media_ic_launcher);
        indicator5.setThumbDrawable(new PixelMapElement(pixelMap));

        indicator7.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_last_next_length_2));

        indicator8.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_last_next_length_2));
        indicator8.customTickTextsTypeface(Font.SERIF);

        indicator9.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_last_next_length_2));

        // set listener
        if (textView != null) {
            textView.setClickedListener(component1 -> {
                Intent intent = new Intent();
                intent.setAction(IntentConstants.ACTION_HOME);
                intent.setUri(Uri.parse(getContext().getString(ResourceTable.String_url_github)));
                mContext.startAbility(intent, 0);
            });
        }

        states.setText("states: ");
        progress.setText("progress: " + indicator11.getProgress());
        progressFloat.setText("progress_float: " + indicator11.getProgressFloat());
        fromUser.setText("from_user: ");
        indicator11.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                states.setText("states: onSeeking");
                progress.setText("progress: " + seekParams.progress);
                progressFloat.setText("progress_float: " + seekParams.progressFloat);
                fromUser.setText("from_user: " + seekParams.fromUser);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
                states.setText("states: onStart");
                progress.setText("progress: " + seekBar.getProgress());
                progressFloat.setText("progress_float: " + seekBar.getProgressFloat());
                fromUser.setText("from_user: true");
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                states.setText("states: onStop");
                progress.setText("progress: " + seekBar.getProgress());
                progressFloat.setText("progress_float: " + seekBar.getProgressFloat());
                fromUser.setText("from_user: false");
            }
        });
    }

    @Override
    public void invalidate() {
        if (isInit) {
            indicator1.invalidate();
            indicator2.invalidate();
            indicator3.invalidate();
            indicator4.invalidate();
            indicator5.invalidate();
            indicator6.invalidate();
            indicator7.invalidate();
            indicator8.invalidate();
            indicator9.invalidate();
            indicator10.invalidate();
            indicator11.invalidate();
            indicator12.invalidate();
        }
    }
}
