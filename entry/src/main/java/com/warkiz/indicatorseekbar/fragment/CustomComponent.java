/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.fragment;

import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.BaseComponent;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Custom界面
 *
 * @since 2021-04-12
 */
public class CustomComponent extends BaseComponent {
    private Context mContext;

    private IndicatorSeekBar indicator1;
    private IndicatorSeekBar indicator2;
    private IndicatorSeekBar indicator3;
    private IndicatorSeekBar indicator4;
    private IndicatorSeekBar indicator5;
    private IndicatorSeekBar indicator6;
    private IndicatorSeekBar indicator7;
    private IndicatorSeekBar indicator8;
    private IndicatorSeekBar indicator9;

    private Text states;
    private Text progress;
    private Text progressFloat;
    private Text fromUser;
    private Text thumbPosition;
    private Text tickText;

    private boolean isInit = false;

    /**
     * CustomComponent
     *
     * @param context 上下文
     */
    public CustomComponent(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_custom;
    }

    @Override
    protected void initView(Component root) {
        mContext = getContext();

        indicator1 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_1);
        indicator2 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_2);
        indicator3 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_3);
        indicator4 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_4);
        indicator5 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_5);
        indicator6 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_6);
        indicator7 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_7);
        indicator8 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_8);
        indicator9 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_9);

        states = (Text) root.findComponentById(ResourceTable.Id_states);
        progress = (Text) root.findComponentById(ResourceTable.Id_progress);
        progressFloat = (Text) root.findComponentById(ResourceTable.Id_progress_float);
        fromUser = (Text) root.findComponentById(ResourceTable.Id_from_user);
        thumbPosition = (Text) root.findComponentById(ResourceTable.Id_thumb_position);
        tickText = (Text) root.findComponentById(ResourceTable.Id_tick_text);
        setView();
        isInit = true;
    }

    private void setView() {
        // set listener
        states.setText("states: ");
        progress.setText("progress: " + indicator4.getProgress());
        progressFloat.setText("progress_float: " + indicator4.getProgressFloat());
        fromUser.setText("from_user: ");
        thumbPosition.setText("thumb_position: ");
        tickText.setText("tick_text: ");
        indicator4.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_tick_below_text_length_6));
        indicator4.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                if (seekParams.fromUser) {
                    states.setText("states: onSeeking");
                } else {
                    states.setText("states: onStop");
                }
                progress.setText("progress: " + seekParams.progress);
                progressFloat.setText("progress_float: " + seekParams.progressFloat);
                fromUser.setText("from_user: " + seekParams.fromUser);
                thumbPosition.setText("thumb_position: " + seekParams.thumbPosition);
                tickText.setText("tick_text: " + seekParams.tickText);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
                states.setText("states: onStart");
                progress.setText("progress: " + seekBar.getProgress());
                progressFloat.setText("progress_float: " + seekBar.getProgressFloat());
                fromUser.setText("from_user: true");
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                states.setText("states: onStop");
                progress.setText("progress: " + seekBar.getProgress());
                progressFloat.setText("progress_float: " + seekBar.getProgressFloat());
                fromUser.setText("from_user: false");
            }
        });
    }

    @Override
    public void invalidate() {
        if (isInit) {
            indicator1.invalidate();
            indicator2.invalidate();
            indicator3.invalidate();
            indicator4.invalidate();
            indicator5.invalidate();
            indicator6.invalidate();
            indicator7.invalidate();
            indicator8.invalidate();
            indicator9.invalidate();
        }
    }
}
