/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.fragment;

import com.warkiz.indicatorseekbar.utils.PixelMapUtil;
import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.BaseComponent;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.IndicatorStayLayout;
import com.warkiz.widget.IndicatorType;
import com.warkiz.widget.SizeUtils;
import com.warkiz.widget.TickMarkType;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentState;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

/**
 * JavaBuild界面
 *
 * @since 2021-04-12
 */
public class JavaBuildComponent extends BaseComponent {
    private Context mContext;

    private IndicatorSeekBar indicator1;
    private IndicatorSeekBar indicator2;
    private IndicatorSeekBar indicator3;
    private IndicatorSeekBar indicator4;
    private IndicatorSeekBar indicator5;
    private IndicatorSeekBar indicator6;
    private IndicatorSeekBar indicator7;
    private IndicatorSeekBar indicator8;

    private boolean isInit = false;

    private DirectionalLayout content;

    private ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer
            .LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
            ComponentContainer.LayoutConfig.MATCH_CONTENT);

    /**
     * JavaBuildComponent
     *
     * @param context 上下文
     */
    public JavaBuildComponent(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_java_build;
    }

    @Override
    protected void initView(Component root) {
        mContext = getContext();
        content = (DirectionalLayout) root.findComponentById(ResourceTable.Id_java_build);

        Text textView1 = getTextView();
        textView1.setText("1. continuous");
        content.addComponent(textView1);

        addIndicator1();

        Text textView2 = getTextView();
        textView2.setText("2. continuous_texts_ends");
        content.addComponent(textView2);

        addIndicator2();

        Text textView3 = getTextView();
        textView3.setText("3. continuous_texts_ends_custom_ripple_thumb");
        content.addComponent(textView3);

        addIndicator3();

        Text textView4 = getTextView();
        textView4.setText("4. continuous_texts_ends_custom");
        content.addComponent(textView4);

        addIndicator4();

        Text textView5 = getTextView();
        textView5.setText("5. discrete_ticks");
        content.addComponent(textView5);

        addIndicator5();

        Text textView6 = getTextView();
        textView6.setText("6. discrete_ticks_texts");
        content.addComponent(textView6);

        addIndicator6();

        Text textView7 = getTextView();
        textView7.setText("7. discrete_ticks_texts_custom");
        content.addComponent(textView7);

        addIndicator7();

        Text textView8 = getTextView();
        textView8.setText("8. discrete_ticks_texts_ends");
        content.addComponent(textView8);

        addIndicator8();

        Text textView9 = getTextView();
        content.addComponent(textView9);
        isInit = true;
    }

    private void addIndicator8() {
        String[] arrayEnds = {"A", "", "", "", "", "", "G"};
        indicator8 = IndicatorSeekBar
                .with(getContext())
                .max(100)
                .min(10)
                .progress(83)
                .tickCount(7)
                .showTickMarksType(TickMarkType.OVAL)
                .tickMarksColor(getSelectorTickMarksColor())
                .tickTextsArray(arrayEnds)
                .showTickTexts(true)
                .showIndicatorType(IndicatorType.CIRCULAR_BUBBLE)
                .tickTextsColorStateList(getSelectorTickTexts3Color())
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .thumbColor(Color.getIntColor("#ff0000"))
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_pink)))
                .trackBackgroundSize(2)
                .build();

        indicator8.setLayoutConfig(layoutConfig);
        addToStayLayout(content, indicator8);
    }

    private void addIndicator7() {
        String[] array = {"A", "B", "C", "D", "E", "F", "G"};
        indicator7 = IndicatorSeekBar
                .with(getContext())
                .max(200)
                .min(10)
                .progress(83)
                .tickCount(7)
                .showTickMarksType(TickMarkType.SQUARE)
                .tickTextsArray(array)
                .showTickTexts(true)
                .tickTextsColorStateList(getSelectorTickTexts3Color())
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.RECTANGLE)
                .thumbColor(Color.getIntColor("#ff0000"))
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();
        indicator7.setLayoutConfig(layoutConfig);
        content.addComponent(indicator7);
    }

    private void addIndicator6() {
        // DISCRETE_TICKS_TEXTS
        indicator6 = IndicatorSeekBar
                .with(getContext())
                .max(110)
                .min(10)
                .progress(53)
                .tickCount(7)
                .showTickMarksType(TickMarkType.OVAL)
                .tickMarksColor(mContext.getColor(ResourceTable.Color_color_blue))
                .tickMarksSize(13)// vp
                .showTickTexts(true)
                .tickTextsColor(mContext.getColor(ResourceTable.Color_color_pink))
                .tickTextsSize(13)// fp
                .tickTextsTypeFace(Font.MONOSPACE)
                .showIndicatorType(IndicatorType.ROUNDED_RECTANGLE)
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .indicatorTextSize(13) // fp
                .thumbColor(mContext.getColor(ResourceTable.Color_colorAccent))
                .thumbSize(14) // vp
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4) // vp
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2) // vp
                .build();
        indicator6.setLayoutConfig(layoutConfig);
        content.addComponent(indicator6);
    }

    private void addIndicator5() {
        // DISCRETE_TICKS
        indicator5 = IndicatorSeekBar
                .with(getContext())
                .max(50)
                .min(10)
                .progress(33)
                .tickCount(7)
                .tickMarksSize(15)
                .tickMarksDrawable(getSelectorTickMarksDrawable())
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.ROUNDED_RECTANGLE)
                .thumbColor(Color.getIntColor("#ff0000"))
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();
        indicator5.setLayoutConfig(layoutConfig);
        addToStayLayout(content, indicator5);
    }

    private void addIndicator4() {
        indicator4 = IndicatorSeekBar
                .with(getContext())
                .max(90)
                .min(10)
                .progress(33)
                .tickCount(2)
                .showTickMarksType(TickMarkType.NONE)
                .showTickTexts(true)
                .tickTextsArray(ResourceTable.Strarray_last_next_length_2)
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.CIRCULAR_BUBBLE)
                .thumbColor(Color.getIntColor("#ff0000"))
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();
        indicator4.setLayoutConfig(layoutConfig);
        content.addComponent(indicator4);
    }

    private void addIndicator3() {
        // CONTINUOUS_TEXTS_ENDS
        indicator3 = IndicatorSeekBar
                .with(getContext())
                .max(100)
                .min(30)
                .progress(33)
                .tickCount(2)
                .showTickMarksType(TickMarkType.NONE)
                .showTickTexts(true)
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.CIRCULAR_BUBBLE)
                .thumbDrawable(getSelectorThumbRippleDrawable())
                .thumbSize(26)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();
        indicator3.setLayoutConfig(layoutConfig);
        content.addComponent(indicator3);
    }

    private void addIndicator2() {
        // CONTINUOUS_TEXTS_ENDS
        indicator2 = IndicatorSeekBar
                .with(getContext())
                .max(100)
                .min(10)
                .progress(33)
                .tickCount(2)
                .showTickMarksType(TickMarkType.NONE)
                .showTickTexts(true)
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.RECTANGLE)
                .thumbDrawable(getSelectorThumbDrawable())
                .thumbSize(18)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();
        indicator2.setLayoutConfig(layoutConfig);
        addToStayLayout(content, indicator2);
    }

    private void addIndicator1() {
        // CONTINUOUS
        indicator1 = IndicatorSeekBar
                .with(getContext())
                .max(200)
                .min(10)
                .progress(33)
                .indicatorColor(new Color(mContext.getColor(ResourceTable.Color_color_blue)))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .showIndicatorType(IndicatorType.CIRCULAR_BUBBLE)
                .thumbColorStateList(getSelectorThumbColor())
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .showThumbText(true)
                .thumbTextColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .build();
        indicator1.setLayoutConfig(layoutConfig);
        content.addComponent(indicator1);
    }

    private void addToStayLayout(DirectionalLayout layout, IndicatorSeekBar seekBar) {
        IndicatorStayLayout stayLayout = new IndicatorStayLayout(getContext());
        stayLayout.attachTo(seekBar);
        stayLayout.setLayoutConfig(layoutConfig);
        layout.addComponent(stayLayout);
    }

    private StateElement getSelectorTickMarksColor() {
        StateElement stateElement = new StateElement();
        ShapeElement selectedElement = new ShapeElement();
        selectedElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_colorAccent)));
        ShapeElement emptyElement = new ShapeElement();
        emptyElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_color_gray)));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, selectedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    private StateElement getSelectorTickTexts3Color() {
        ShapeElement selectedElement = new ShapeElement();
        selectedElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_colorAccent)));
        ShapeElement hoveredElement = new ShapeElement();
        hoveredElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_color_blue)));
        ShapeElement emptyElement = new ShapeElement();
        emptyElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_color_gray)));
        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, selectedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_HOVERED}, hoveredElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    private StateElement getSelectorTickMarksDrawable() {
        StateElement stateElement = new StateElement();
        PixelMapElement selectedElement = new PixelMapElement(PixelMapUtil.getPixelMap(mContext,
                ResourceTable.Media_ic_launcher_round));
        PixelMapElement emptyElement = new PixelMapElement(PixelMapUtil.getPixelMap(mContext,
                ResourceTable.Media_ic_launcher));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_SELECTED}, selectedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    private Element getSelectorThumbRippleDrawable() {
        StateElement stateElement = new StateElement();
        ShapeElement pressedElement = new ShapeElement(mContext, ResourceTable.Graphic_custom_indicator_bg_oval_alpha);
        ShapeElement emptyElement = new ShapeElement(mContext, ResourceTable.Graphic_custom_thumb_bg_oval_alpha);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, pressedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    private StateElement getSelectorThumbDrawable() {
        StateElement stateElement = new StateElement();
        ShapeElement pressedElement = new ShapeElement(mContext, ResourceTable.Graphic_custom_indicator_bg_oval);
        PixelMapElement emptyElement = new PixelMapElement(PixelMapUtil.getPixelMap(mContext,
                ResourceTable.Media_ic_launcher));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, pressedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    private StateElement getSelectorThumbColor() {
        StateElement stateElement = new StateElement();
        ShapeElement pressedElement = new ShapeElement();
        ShapeElement emptyElement = new ShapeElement();
        pressedElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_colorAccent)));
        emptyElement.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(ResourceTable.Color_color_blue)));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, pressedElement);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
        return stateElement;
    }

    @Override
    public void invalidate() {
        if (isInit) {
            indicator1.invalidate();
            indicator2.invalidate();
            indicator3.invalidate();
            indicator4.invalidate();
            indicator5.invalidate();
            indicator6.invalidate();
            indicator7.invalidate();
            indicator8.invalidate();
        }
    }

    private Text getTextView() {
        Text textView = new Text(mContext);
        DirectionalLayout.LayoutConfig config = new DirectionalLayout
                .LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        config.alignment = LayoutAlignment.LEFT;
        textView.setLayoutConfig(config);
        int padding = vp2px(mContext, 16);
        textView.setPadding(padding, padding, padding, 0);
        textView.setTextSize(SizeUtils.fp2px(mContext, 13));
        textView.setTextColor(new Color(Color.getIntColor("#555555")));
        return textView;
    }

    private int vp2px(Context context, float vpValue) {
        return AttrHelper.vp2px(vpValue, context);
    }
}
