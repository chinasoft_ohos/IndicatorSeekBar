/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.fragment;

import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.BaseComponent;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.IndicatorStayLayout;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Indicator界面
 *
 * @since 2021-04-12
 */
public class IndicatorComponent extends BaseComponent {
    private Context mContext;

    private IndicatorSeekBar indicator1;
    private IndicatorSeekBar indicator2;
    private IndicatorSeekBar indicator3;
    private IndicatorSeekBar indicator4;
    private IndicatorSeekBar indicator5;
    private IndicatorSeekBar indicator6;

    private IndicatorStayLayout stayLayout1;
    private IndicatorStayLayout stayLayout2;
    private IndicatorStayLayout stayLayout3;
    private IndicatorStayLayout stayLayout4;
    private IndicatorStayLayout stayLayout5;
    private IndicatorStayLayout stayLayout6;

    private boolean isInit = false;

    /**
     * IndicatorComponent
     *
     * @param context 上下文
     */
    public IndicatorComponent(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_custom_indicator;
    }

    @Override
    protected void initView(Component root) {
        mContext = getContext();

        indicator1 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_1);
        indicator2 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_2);
        indicator3 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_3);
        indicator4 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_4);
        indicator5 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_5);
        indicator6 = (IndicatorSeekBar) root.findComponentById(ResourceTable.Id_indicator_6);

        stayLayout1 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_1);
        stayLayout2 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_2);
        stayLayout3 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_3);
        stayLayout4 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_4);
        stayLayout5 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_5);
        stayLayout6 = (IndicatorStayLayout) root.findComponentById(ResourceTable.Id_stay_layout_6);
        Component milesComponent = LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_custom_top_content_view_round_miles, null, false);
        Text milesIsbProgress = (Text) milesComponent.findComponentById(ResourceTable.Id_isb_progress);
        indicator1.getIndicator().setTopContentView(milesComponent, milesIsbProgress);

        // custom indicator text by java code
        indicator2.setIndicatorTextFormat("${PROGRESS} %");

        // custom indicator text by java code
        indicator3.setIndicatorTextFormat("${TICK_TEXT} --");
        indicator3.customTickTexts(mContext.getStringArray(ResourceTable.Strarray_tick_below_text_length_5));

        indicator4.setShowIndicatorType(4);
        Component ovalComponent = LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_custom_indicator_oval, null, false);
        indicator4.setIndicatorContentView(ovalComponent);
        indicator4.getIndicator().initIndicator();

        Component roundComponent = LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_custom_top_content_view_round, null, false);
        Text roundIsbProgress = (Text) roundComponent.findComponentById(ResourceTable.Id_isb_progress);
        indicator5.getIndicator().setTopContentView(roundComponent, roundIsbProgress);

        Component rectComponent = LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_custom_top_content_view_rect, null, false);
        Text rectIsbProgress = (Text) rectComponent.findComponentById(ResourceTable.Id_isb_progress);
        indicator6.getIndicator().setTopContentView(rectComponent, rectIsbProgress);

        stayLayout1.layoutIndicator(indicator1, 0);
        stayLayout2.layoutIndicator(indicator2, 0);
        stayLayout3.layoutIndicator(indicator3, 0);
        stayLayout4.layoutIndicator(indicator4, 0);
        stayLayout5.layoutIndicator(indicator5, 0);
        stayLayout6.layoutIndicator(indicator6, 0);
        isInit = true;
    }

    @Override
    public void invalidate() {
        if (isInit) {
            indicator1.invalidate();
            indicator2.invalidate();
            indicator3.invalidate();
            indicator4.invalidate();
            indicator5.invalidate();
            indicator6.invalidate();
        }
    }
}
