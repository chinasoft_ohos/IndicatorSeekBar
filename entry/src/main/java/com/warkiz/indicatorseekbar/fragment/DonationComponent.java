/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.indicatorseekbar.fragment;

import com.warkiz.indicatorseekbar.ResourceTable;
import com.warkiz.indicatorseekbar.donation.AlipayUtil;
import com.warkiz.indicatorseekbar.donation.BaseComponent;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

/**
 * Donation界面
 *
 * @since 2021-04-12
 */
public class DonationComponent extends BaseComponent implements Component.ClickedListener {
    private Context mContext;

    private Image mAlipayQrCode;
    private Image mWechatpayQrCode;

    /**
     * DonationComponent
     *
     * @param context 上下文
     */
    public DonationComponent(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_buy_me_a_coffee;
    }

    @Override
    protected void initView(Component root) {
        mContext = getContext();
        root.findComponentById(ResourceTable.Id_alipay_text).setClickedListener(this);
        root.findComponentById(ResourceTable.Id_wechatpay_text).setClickedListener(this);
        root.findComponentById(ResourceTable.Id_paypal_text).setClickedListener(this);
        mAlipayQrCode = (Image) root.findComponentById(ResourceTable.Id_alipay_qr_code);
        mWechatpayQrCode = (Image) root.findComponentById(ResourceTable.Id_wechatpay_qr_code);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_alipay_text:
                alipay();
                break;
            case ResourceTable.Id_wechatpay_text:
                wechatPay();
                break;
            case ResourceTable.Id_paypal_text:
                paypal();
                break;
            default:
                break;
        }
    }

    private void alipay() {
        if (AlipayUtil.hasInstalledAlipayClient(mContext)) {
            AlipayUtil.startAlipayClient(mContext);
        } else {
            mAlipayQrCode.setVisibility(mAlipayQrCode.getVisibility() == Component.VISIBLE
                    ? Component.HIDE : Component.VISIBLE);
        }
    }

    private void wechatPay() {
        mWechatpayQrCode.setVisibility(mWechatpayQrCode.getVisibility() == Component.VISIBLE
                ? Component.HIDE : Component.VISIBLE);
    }

    private void paypal() {
        Intent intents = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withUri(Uri.parse(getContext().getString(ResourceTable.String_url_paypal)))
                .withAction(IntentConstants.ACTION_SEARCH)
                .build();
        intents.setOperation(operation);
        mContext.startAbility(intents, 0);
    }
}
