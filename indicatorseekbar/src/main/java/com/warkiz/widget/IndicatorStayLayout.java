package com.warkiz.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**
 * created by zhuangguangquan on 2018/5/27
 * <p>
 * the container for IndicatorSeekBar to make the indicator stay always
 * <p>
 * https://github.com/warkiz/IndicatorSeekBar
 * <p>
 * Donation/打赏:
 * If this library is helpful to you ,you can give me a donation by:
 *
 * @see <a href="https://www.paypal.me/BuyMeACupOfTeaThx">ZhuanGuangQuan's Paypal</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/wechat_pay.png?raw=true">微信支付</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/alipay.png?raw=true">支付宝</a>
 * <p>
 */
public class IndicatorStayLayout extends DirectionalLayout {

    public IndicatorStayLayout(Context context) {
        this(context, null);
    }

    public IndicatorStayLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public IndicatorStayLayout(Context context, AttrSet attrSet, String defStyleAttr) {
        super(context, attrSet, defStyleAttr);
        setOrientation(VERTICAL);
    }

    /**
     * If you want to initial seek bar by java code to make
     * indicator stay always,call this.
     *
     * @param seekBar the direct child in indicatorStayLayout
     */
    public void attachTo(IndicatorSeekBar seekBar) {
        attachTo(seekBar, -2);
    }

    /**
     * If you want to initial seek bar by java code to make
     * indicator stay always,call this.
     *
     * @param seekBar the direct child in indicatorStayLayout
     * @param index   the child index you wanted indicatorSeekBar to attach to IndicatorStayLayout;
     */
    public void attachTo(IndicatorSeekBar seekBar, int index) {
        if (seekBar == null) {
            throw new NullPointerException("the seek bar wanna attach to IndicatorStayLayout " +
                    "can not be null value.");
        }
        layoutIndicator(seekBar, index);
        addComponent(seekBar, index + 1);
    }

    /**
     * layout each indicator
     *
     * @param child the indicatorSeekBar which should hava a indicator content view.
     * @param index the index you want the seek bar to located in IndicatorStayLayout.
     */
    public void layoutIndicator(Component child, int index) {
        if (child instanceof IndicatorSeekBar) {
            IndicatorSeekBar seekBar = (IndicatorSeekBar) child;
            seekBar.setIndicatorStayAlways(true);
            Component contentView = seekBar.getIndicatorContentView();
            if (contentView == null) {
                throw new IllegalStateException("Can not find any indicator in the IndicatorSeekBar, please " +
                        "make sure you have called the attr: SHOW_INDICATOR_TYPE for IndicatorSeekBar and the value is not IndicatorType.NONE.");
            }
            if (contentView instanceof IndicatorSeekBar) {
                throw new IllegalStateException("IndicatorSeekBar can not be a contentView for Indicator in case this inflating loop.");
            }
            LayoutConfig layoutConfig = new LayoutConfig(
                            ComponentContainer.LayoutConfig.MATCH_CONTENT,
                            ComponentContainer.LayoutConfig.MATCH_CONTENT);
            layoutConfig.setMarginBottom(SizeUtils.vp2px(seekBar.getContext(), 2) - seekBar.getPaddingTop());
            addComponent(contentView, index, layoutConfig);
            seekBar.showStayIndicator();
        }
    }

    @Override
    public void setOrientation(int orientation) {
        if (orientation != VERTICAL) {
            throw new IllegalArgumentException("IndicatorStayLayout is always vertical and does"
                    + " not support horizontal orientation");
        }
        super.setOrientation(orientation);
    }

}
