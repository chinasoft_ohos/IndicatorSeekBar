package com.warkiz.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * created by zhuangguangquan on 2017/12/13.
 * <p>
 * https://github.com/warkiz/IndicatorSeekBar
 * <p>
 * Donation/打赏:
 * If this library is helpful to you ,you can give me a donation by:
 *
 * @see <a href="https://www.paypal.me/BuyMeACupOfTeaThx">ZhuanGuangQuan's Paypal</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/wechat_pay.png?raw=true">微信支付</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/alipay.png?raw=true">支付宝</a>
 * <p>
 */

public class CircleBubbleView extends Component implements Component.EstimateSizeListener {
    private Color mIndicatorTextColor;
    private Color mIndicatorColor;
    private int mIndicatorTextSize;
    private Context mContext;
    private Path mPath;
    private Paint mPaint;
    private float mIndicatorWidth;
    private float mIndicatorHeight;
    private float mTextHeight;
    private String mProgress;

    public CircleBubbleView(Context context) {
        this(context, null);
    }

    public CircleBubbleView(Context context, AttrSet attrSet) {
        this(context, attrSet, 0);
    }

    public CircleBubbleView(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init("100");
    }

    public CircleBubbleView(Context context, int indicatorTextSize, Color indicatorTextColor, Color indicatorColor, String maxLengthText) {
        super(context, null, 0);
        this.mContext = context;
        this.mIndicatorTextSize = indicatorTextSize;
        this.mIndicatorTextColor = indicatorTextColor;
        this.mIndicatorColor = indicatorColor;
        init(maxLengthText);
    }

    private void init(String maxLengthText) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(1);
        mPaint.setTextAlign(TextAlignment.CENTER);
        mPaint.setTextSize(mIndicatorTextSize);
        Rect mRect = mPaint.getTextBounds(maxLengthText);
        mIndicatorWidth = mRect.getWidth() + SizeUtils.vp2px(mContext, 4);
        int minWidth = SizeUtils.vp2px(mContext, 36);
        if (mIndicatorWidth < minWidth) {
            mIndicatorWidth = minWidth;
        }
        mTextHeight = mRect.getHeight();
        mIndicatorHeight = mIndicatorWidth * 1.2f;
        initPath();

        setEstimateSizeListener(this);
        addDrawTask(new DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                mPaint.setColor(mIndicatorColor);
                canvas.drawPath(mPath, mPaint);
                mPaint.setColor(mIndicatorTextColor);
                canvas.drawText(mPaint, mProgress, mIndicatorWidth / 2f, mIndicatorHeight / 2 + mTextHeight / 4);
            }
        });
    }

    private void initPath() {
        mPath = new Path();
        RectFloat rectF = new RectFloat(0, 0, mIndicatorWidth, mIndicatorWidth);
        mPath.arcTo(rectF, 135, 270);
        mPath.lineTo(mIndicatorWidth / 2, mIndicatorHeight);
        mPath.close();
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        int width = Component.EstimateSpec.getSize((int) mIndicatorWidth);
        int height = Component.EstimateSpec.getSize((int) mIndicatorHeight);
        setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
        return true;
    }

    public void setProgress(String progress) {
        this.mProgress = progress;
        invalidate();
    }
}
