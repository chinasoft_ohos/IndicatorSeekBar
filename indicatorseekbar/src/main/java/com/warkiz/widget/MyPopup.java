/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.warkiz.widget;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.DependentLayout;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 自定义浮窗
 *
 * @since 2021-04-12
 */
public class MyPopup {
    private Context mContext;

    private Component parent;

    private Component indicatorView;

    private int mGap;

    /**
     * MyPopup
     *
     * @param context 上下文
     * @param seekbar 要显示的浮窗的控件
     * @param indicatorView 自定义的提示控件
     */
    public MyPopup(Context context, Component seekbar, Component indicatorView) {
        mContext = context;
        mGap = SizeUtils.vp2px(mContext, 2);
        parent = getComponentParent(seekbar);
        this.indicatorView = indicatorView;
        this.indicatorView.setVisibility(Component.HIDE);
    }

    /**
     * 显示在某个控件的上方，偏移量为x
     *
     * @param component 父控件
     * @param x 偏移量
     */
    public void show(Component component, float x) {
        indicatorView.setVisibility(Component.INVISIBLE);
        if (this.indicatorView.getComponentParent() == null) {
            ((ComponentContainer) parent).addComponent(this.indicatorView);
        }
        int estimatedWidth = indicatorView.getEstimatedWidth();
        float translationX;
        if (((int) x + estimatedWidth / 2) > getWindowWidth()) {
            translationX = getWindowWidth() - estimatedWidth;
        } else if (((int) x - estimatedWidth / 2) < 0) {
            translationX = 0;
        } else {
            translationX = (int) x - estimatedWidth / 2;
        }
        indicatorView.setTranslation(translationX,
            component.getContentPositionY() - indicatorView.getEstimatedHeight() - mGap);
        update(component, x);
    }

    /**
     * 更新在某个控件的上方，偏移量为x
     *
     * @param component 父控件
     * @param x 偏移量
     */
    public void update(Component component, float x) {
        indicatorView.setVisibility(Component.VISIBLE);
        int estimatedWidth = indicatorView.getEstimatedWidth();
        float translationX;
        if (((int) x + estimatedWidth / 2) > getWindowWidth()) {
            translationX = getWindowWidth() - estimatedWidth;
        } else if (((int) x - estimatedWidth / 2) < 0) {
            translationX = 0;
        } else {
            translationX = (int) x - estimatedWidth / 2;
        }
        indicatorView.setTranslation(translationX,
            component.getContentPositionY() - indicatorView.getEstimatedHeight() - mGap);
        indicatorView.invalidate();
        ArrowView mArrowView = (ArrowView) indicatorView.findComponentById(ResourceTable.Id_indicator_arrow);
        if (mArrowView != null) {
            mArrowView.invalidate();
        }
    }

    private int getWindowWidth() {
        Display display = DisplayManager.getInstance().getDefaultDisplay(mContext).get();
        return display.getAttributes().width;
    }

    private Component getComponentParent(Component component) {
        ComponentParent componentParent = component.getComponentParent();
        if (componentParent instanceof DependentLayout) {
            return (Component) componentParent;
        } else {
            return getComponentParent((Component) componentParent);
        }
    }

    /**
     * 隐藏浮窗
     */
    public void hide() {
        indicatorView.setVisibility(Component.HIDE);
        ((ComponentContainer) parent).removeComponent(indicatorView);
    }

    /**
     * 浮窗是否显示
     *
     * @return 返回值
     */
    public boolean isShowing() {
        if (indicatorView != null) {
            if (indicatorView.getVisibility() == Component.VISIBLE) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public Component getContentView() {
        return indicatorView;
    }
}
