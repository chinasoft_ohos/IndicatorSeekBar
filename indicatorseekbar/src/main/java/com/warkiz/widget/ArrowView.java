package com.warkiz.widget;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * created by zhuangguangquan on 2017/9/6
 * <p>
 * https://github.com/warkiz/IndicatorSeekBar
 * <p>
 * Donation/打赏:
 * If this library is helpful to you ,you can give me a donation by:
 *
 * @see <a href="https://www.paypal.me/BuyMeACupOfTeaThx">ZhuanGuangQuan's Paypal</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/wechat_pay.png?raw=true">微信支付</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/alipay.png?raw=true">支付宝</a>
 * <p>
 */

public class ArrowView extends Component {
    private int mWidth;
    private int mHeight;
    private Path mPath;
    private Paint mPaint;

    private DrawTask mDrawTask = new DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            canvas.drawPath(mPath, mPaint);
        }
    };

    public ArrowView(Context context) {
        this(context, null);
    }

    public ArrowView(Context context, AttrSet attrSet) {
        this(context, attrSet, 0);
    }

    public ArrowView(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        mWidth = AttrHelper.vp2px(12, context);
        mHeight = AttrHelper.vp2px(7, context);
        mPath = new Path();
        mPath.moveTo(0, 0);
        mPath.lineTo(mWidth, 0);
        mPath.lineTo(mWidth / 2f, mHeight);
        mPath.close();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(1);

        setEstimateSizeListener((widthMeasureSpec, heightMeasureSpec) -> {
            int width = Component.EstimateSpec.getSize(mWidth);
            int height = Component.EstimateSpec.getSize(mHeight);
            setEstimatedSize(
                    Component.EstimateSpec.getChildSizeWithMode(width, width, Component.EstimateSpec.NOT_EXCEED),
                    Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));
            return true;
        });
        addDrawTask(mDrawTask);
    }

    public void setColor(int color) {
        mPaint.setColor(new Color(color));
        invalidate();
    }
}
