package com.warkiz.widget;

import ohos.agp.components.AttrHelper;
import ohos.app.Context;

/**
 * created by zhuangguangquan on  2017/9/9
 */

public class SizeUtils {
    public static int vp2px(Context context, float vpValue) {
        return AttrHelper.vp2px(vpValue, context);
    }

    public static int fp2px(Context context, float fpValue) {
        return (int) (fpValue * AttrHelper.getDensity(context) + 0.5f);
    }

    public static int px2fp(Context context, float pxValue) {
        return (int) (pxValue / AttrHelper.getDensity(context) + 0.5f);
    }

}
