package com.warkiz.widget;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.StateElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * created by zhuangguangquan on 2018/6/3
 * <p>
 * https://github.com/warkiz/IndicatorSeekBar
 * <p>
 * Donation/打赏:
 * If this library is helpful to you ,you can give me a donation by:
 *
 * @see <a href="https://www.paypal.me/BuyMeACupOfTeaThx">ZhuanGuangQuan's Paypal</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/wechat_pay.png?raw=true">微信支付</a>, or
 * @see <a href="https://github.com/warkiz/IndicatorSeekBar/blob/master/app/src/main/res/mipmap-xxhdpi/alipay.png?raw=true">支付宝</a>
 * <p>
 */

public class Builder {
    final Context context;
    //seek bar
    float max = 100;
    float min = 0;
    float progress = 0;
    boolean progressValueFloat = false;
    boolean seekSmoothly = false;
    boolean r2l = false;
    boolean userSeekable = true;
    boolean onlyThumbDraggable = false;
    boolean clearPadding = false;
    //indicator
    int showIndicatorType = IndicatorType.ROUNDED_RECTANGLE;
    Color indicatorColor = new Color(Color.getIntColor("#FF4081"));
    Color indicatorTextColor = new Color(Color.getIntColor("#FFFFFF"));
    int indicatorTextSize = 0;
    Component indicatorContentView = null;
    Component indicatorTopContentView = null;
    //track

    int trackBackgroundSize = 0;
    Color trackBackgroundColor = new Color(Color.getIntColor("#D7D7D7"));
    int trackProgressSize = 0;
    Color trackProgressColor = new Color(Color.getIntColor("#FF4081"));
    boolean trackRoundedCorners = false;
    //thumbText
    Color thumbTextColor = new Color(Color.getIntColor("#FF4081"));
    boolean showThumbText = false;
    //thumb
    int thumbSize = 0;
    int thumbColor = Color.getIntColor("#FF4081");
    StateElement thumbColorStateList = null;
    Element thumbDrawable = null;
    //tickTexts
    boolean showTickText;
    int tickTextsColor = Color.getIntColor("#FF4081");
    int tickTextsSize = 0;
    String[] tickTextsCustomArray = null;
    Font tickTextsTypeFace = Font.DEFAULT;
    StateElement tickTextsColorStateList = null;
    //tickMarks
    int tickCount = 0;
    int showTickMarksType = TickMarkType.NONE;
    int tickMarksColor = Color.getIntColor("#FF4081");
    int tickMarksSize = 0;
    Element tickMarksDrawable = null;
    boolean tickMarksEndsHide = false;
    boolean tickMarksSweptHide = false;
    StateElement tickMarksColorStateList = null;

    Builder(Context context) {
        this.context = context;
        this.indicatorTextSize = SizeUtils.fp2px(context, 14);
        this.trackBackgroundSize = SizeUtils.vp2px(context, 2);
        this.trackProgressSize = SizeUtils.vp2px(context, 2);
        this.tickMarksSize = SizeUtils.vp2px(context, 10);
        this.tickTextsSize = SizeUtils.fp2px(context, 13);
        this.thumbSize = SizeUtils.vp2px(context, 14);
    }

    /**
     * call this to new an IndicatorSeekBar
     *
     * @return IndicatorSeekBar
     */
    public IndicatorSeekBar build() {
        return new IndicatorSeekBar(this);
    }

    /**
     * Set the upper limit of this seek bar's range.
     *
     * @param max the max range
     * @return Builder
     */
    public Builder max(float max) {
        this.max = max;
        return this;
    }

    /**
     * Set the  lower limit of this seek bar's range.
     *
     * @param min the min range
     * @return Builder
     */
    public Builder min(float min) {
        this.min = min;
        return this;
    }

    /**
     * Sets the current progress to the specified value.
     *
     * @param progress the current level of seek bar
     * @return Builder
     */
    public Builder progress(float progress) {
        this.progress = progress;
        return this;
    }

    /**
     * make the progress in float type. default in int type.
     *
     * @param isFloatProgress true for float progress,default false.
     * @return Builder
     */
    public Builder progressValueFloat(boolean isFloatProgress) {
        this.progressValueFloat = isFloatProgress;
        return this;
    }

    /**
     * seek continuously or discrete
     *
     * @param seekSmoothly true for seek continuously ignore having tickMarks.
     * @return Builder
     */
    public Builder seekSmoothly(boolean seekSmoothly) {
        this.seekSmoothly = seekSmoothly;
        return this;
    }

    /**
     * right to left,compat local problem.
     *
     * @param r2l true for local which read text from right to left
     * @return Builder
     */
    public Builder r2l(boolean r2l) {
        this.r2l = r2l;
        return this;
    }

    /**
     * seek bar has a default padding left and right(16 dp) , call this method to set both to zero.
     *
     * @param clearPadding true to clear the default padding, false to keep.
     * @return Builder
     */
    public Builder clearPadding(boolean clearPadding) {
        this.clearPadding = clearPadding;
        return this;
    }

    /**
     * prevent user from touching to seek or not
     *
     * @param userSeekable true user can seek.
     * @return Builder
     */
    public Builder userSeekable(boolean userSeekable) {
        this.userSeekable = userSeekable;
        return this;
    }

    /**
     * user change the thumb's location by touching thumb,touching track will not worked.
     *
     * @param onlyThumbDraggable true for seeking only by drag thumb. default false;
     * @return Builder
     */
    public Builder onlyThumbDraggable(boolean onlyThumbDraggable) {
        this.onlyThumbDraggable = onlyThumbDraggable;
        return this;
    }

    /**
     * call this method to show different shape of indicator.
     *
     * @param showIndicatorType see{@link IndicatorType}
     *                          IndicatorType.NONE;
     *                          IndicatorType.CIRCULAR_BUBBLE;
     *                          IndicatorType.ROUNDED_RECTANGLE;
     *                          IndicatorType.RECTANGLE;
     *                          IndicatorType.CUSTOM;
     * @return Builder
     */
    public Builder showIndicatorType(int showIndicatorType) {
        this.showIndicatorType = showIndicatorType;
        return this;
    }

    /**
     * set the seek bar's indicator's color. have no influence on custom indicator.
     *
     * @param indicatorColor colorInt
     * @return Builder
     */
    public Builder indicatorColor(Color indicatorColor) {
        this.indicatorColor = indicatorColor;
        return this;
    }

    /**
     * set the color for indicator text . have no influence on custom tickDrawable.
     *
     * @param indicatorTextColor ColorInt
     * @return Builder
     */
    public Builder indicatorTextColor(Color indicatorTextColor) {
        this.indicatorTextColor = indicatorTextColor;
        return this;
    }

    /**
     * change the size for indicator text.have no influence on custom indicator.
     *
     * @param indicatorTextSize The scaled pixel size.
     * @return Builder
     */
    public Builder indicatorTextSize(int indicatorTextSize) {
        this.indicatorTextSize = SizeUtils.fp2px(context, indicatorTextSize);
        return this;
    }

    /**
     * set the seek bar's indicator's custom indicator view. only effect on custom indicator type.
     *
     * @param indicatorContentView the custom indicator view
     * @return Builder
     */
    public Builder indicatorContentView(Component indicatorContentView) {
        this.indicatorContentView = indicatorContentView;
        return this;
    }

    /**
     * set the seek bar's indicator's custom indicator layout identify. only effect on custom indicator type.
     *
     * @param layoutId the custom indicator layout identify
     * @return Builder
     */
    public Builder indicatorContentViewLayoutId(int layoutId) {
        this.indicatorContentView = LayoutScatter.getInstance(context).parse(layoutId, null, false);
        return this;
    }

    /**
     * set the seek bar's indicator's custom top content view.
     * no effect on custom and circular_bubble indicator type.
     *
     * @param topContentView the custom indicator top content view
     * @return Builder
     */
    public Builder indicatorTopContentView(Component topContentView) {
        this.indicatorTopContentView = topContentView;
        return this;
    }

    /**
     * set the seek bar's indicator's custom top content view layout identify.
     * no effect on custom and circular_bubble indicator type.
     *
     * @param layoutId the custom view for indicator top content layout identify.
     * @return Builder
     */
    public Builder indicatorTopContentViewLayoutId(int layoutId) {
        this.indicatorTopContentView = LayoutScatter.getInstance(context).parse(layoutId, null, false);
        return this;
    }


    /**
     * set the seek bar's background track's Stroke Width
     *
     * @param trackBackgroundSize The dp size.
     * @return Builder
     */
    public Builder trackBackgroundSize(int trackBackgroundSize) {
        this.trackBackgroundSize = SizeUtils.vp2px(context, trackBackgroundSize);
        return this;
    }

    /**
     * set the seek bar's background track's color.
     *
     * @param trackBackgroundColor colorInt
     * @return Builder
     */
    public Builder trackBackgroundColor(Color trackBackgroundColor) {
        this.trackBackgroundColor = trackBackgroundColor;
        return this;
    }

    /**
     * set the seek bar's progress track's Stroke Width
     *
     * @param trackProgressSize The dp size.
     * @return Builder
     */
    public Builder trackProgressSize(int trackProgressSize) {
        this.trackProgressSize = SizeUtils.vp2px(context, trackProgressSize);
        return this;
    }

    /**
     * set the seek bar's progress track's color.
     *
     * @param trackProgressColor colorInt
     * @return Builder
     */
    public Builder trackProgressColor(Color trackProgressColor) {
        this.trackProgressColor = trackProgressColor;
        return this;
    }

    /**
     * call this method to show the seek bar's ends to square corners.default rounded corners.
     *
     * @param trackRoundedCorners false to show square corners.
     * @return Builder
     */
    public Builder trackRoundedCorners(boolean trackRoundedCorners) {
        this.trackRoundedCorners = trackRoundedCorners;
        return this;
    }

    /**
     * set the seek bar's thumb's text color.
     *
     * @param thumbTextColor colorInt
     * @return Builder
     */
    public Builder thumbTextColor(Color thumbTextColor) {
        this.thumbTextColor = thumbTextColor;
        return this;
    }

    /**
     * call this method to show the text below thumb or not
     *
     * @param showThumbText show the text below thumb or not
     * @return Builder
     */
    public Builder showThumbText(boolean showThumbText) {
        this.showThumbText = showThumbText;
        return this;
    }

    /**
     * set the seek bar's thumb's color.
     *
     * @param thumbColor colorInt
     * @return Builder
     */
    public Builder thumbColor(int thumbColor) {
        this.thumbColor = thumbColor;
        return this;
    }

    /**
     * set the seek bar's thumb's selector color.
     *
     * @param thumbColorStateList color selector
     * @return Builder
     * selector format like:
     */
    //<?xml version="1.0" encoding="utf-8"?>
    //<selector xmlns:ohos="http://schemas.ohos.com/apk/res/ohos">
    //<item ohos:color="@color/colorAccent" ohos:state_pressed="true" />  <!--this color is for thumb which is at pressing status-->
    //<item ohos:color="@color/color_blue" />                                <!--for thumb which is at normal status-->
    //</selector>
    public Builder thumbColorStateList(StateElement thumbColorStateList) {
        this.thumbColorStateList = thumbColorStateList;
        return this;
    }

    /**
     * set the seek bar's thumb's Width.will be limited in 30dp.
     *
     * @param thumbSize The dp size.
     * @return Builder
     */
    public Builder thumbSize(int thumbSize) {
        this.thumbSize = SizeUtils.vp2px(context, thumbSize);
        return this;
    }

    /**
     * set a new thumb drawable.
     *
     * @param thumbDrawable the drawable for thumb.
     * @return Builder
     */
    public Builder thumbDrawable(Element thumbDrawable) {
        this.thumbDrawable = thumbDrawable;
        return this;
    }

    /**
     * call this method to custom the thumb showing drawable by selector Drawable.
     *
     * @param thumbStateListDrawable the drawable show as Thumb.
     * @return Builder
     * <p>
     * selector format:
     */
    //<?xml version="1.0" encoding="utf-8"?>
    //<selector xmlns:ohos="http://schemas.ohos.com/apk/res/ohos">
    //<item ohos:drawable="Your drawableA" ohos:state_pressed="true" />  <!--this drawable is for thumb when pressing-->
    //<item ohos:drawable="Your drawableB" />  < !--for thumb when normal-->
    //</selector>
    /* public Builder thumbDrawable(Element thumbStateListDrawable) {
        this.thumbDrawable = thumbStateListDrawable;
        return this;
    }*/


    /**
     * show the tick texts or not
     *
     * @param showTickText show the text below track or not.
     * @return Builder
     */
    public Builder showTickTexts(boolean showTickText) {
        this.showTickText = showTickText;
        return this;
    }

    /**
     * set the color for text below/above seek bar's tickText.
     *
     * @param tickTextsColor ColorInt
     * @return Builder
     */
    public Builder tickTextsColor(int tickTextsColor) {
        this.tickTextsColor = tickTextsColor;
        return this;
    }

    /**
     * set the selector color for text below/above seek bar's tickText.
     *
     * @param tickTextsColorStateList ColorInt
     * @return Builder
     * selector format like:
     */
    //<?xml version="1.0" encoding="utf-8"?>
    //<selector xmlns:ohos="http://schemas.ohos.com/apk/res/ohos">
    //<item ohos:color="@color/colorAccent" ohos:state_selected="true" />  <!--this color is for texts those are at left side of thumb-->
    //<item ohos:color="@color/color_blue" ohos:state_hovered="true" />     <!--for thumb below text-->
    //<item ohos:color="@color/color_gray" />                                 <!--for texts those are at right side of thumb-->
    //</selector>
    public Builder tickTextsColorStateList(StateElement tickTextsColorStateList) {
        this.tickTextsColorStateList = tickTextsColorStateList;
        return this;
    }

    /**
     * set the size for tickText which below/above seek bar's tick .
     *
     * @param tickTextsSize The scaled pixel size.
     * @return Builder
     */
    public Builder tickTextsSize(int tickTextsSize) {
        this.tickTextsSize = SizeUtils.fp2px(context, tickTextsSize);
        return this;
    }

    /**
     * call this method to replace the seek bar's tickMarks' below/above tick texts.
     *
     * @param tickTextsArray the length should same as tickCount.
     * @return Builder
     */
    public Builder tickTextsArray(String[] tickTextsArray) {
        if (tickTextsArray != null) {
            this.tickTextsCustomArray = tickTextsArray.clone();
        }
        return this;
    }


    /**
     * call this method to replace the seek bar's tickMarks' below/above tick texts.
     *
     * @param tickTextsArray the length should same as tickNum.
     * @return Builder
     */
    public Builder tickTextsArray(int tickTextsArray) {
        this.tickTextsCustomArray = context.getStringArray(tickTextsArray);
        return this;
    }

    /**
     * set the tick text's / thumb text textTypeface .
     *
     * @param tickTextsTypeFace The text textTypeface.
     * @return Builder
     */
    public Builder tickTextsTypeFace(Font tickTextsTypeFace) {
        this.tickTextsTypeFace = tickTextsTypeFace;
        return this;
    }

    /**
     * set the tickMarks number.
     *
     * @param tickCount the tickMarks count show on seek bar.
     *                  if you want the seek bar's block size is N , this tickCount should be N+1.
     * @return Builder
     */
    public Builder tickCount(int tickCount) {
        this.tickCount = tickCount;
        return this;
    }

    /**
     * call this method to show different tickMark shape.
     *
     * @param tickMarksType see{@link TickMarkType}
     *                      TickMarkType.NONE;
     *                      TickMarkType.OVAL;
     *                      TickMarkType.SQUARE;
     *                      TickMarkType.DIVIDER;
     * @return Builder
     */
    public Builder showTickMarksType(int tickMarksType) {
        this.showTickMarksType = tickMarksType;
        return this;
    }

    /**
     * set the seek bar's tick's color.
     *
     * @param tickMarksColor colorInt
     * @return Builder
     */
    public Builder tickMarksColor(int tickMarksColor) {
        this.tickMarksColor = tickMarksColor;
        return this;
    }

    /**
     * set the seek bar's tick's color.
     *
     * @param tickMarksColorStateList colorInt
     * @return Builder
     * selector format like:
     */
    //<?xml version="1.0" encoding="utf-8"?>
    //<selector xmlns:ohos="http://schemas.ohos.com/apk/res/ohos">
    //<item ohos:color="@color/colorAccent" ohos:state_selected="true" />  <!--this color is for marks those are at left side of thumb-->
    //<item ohos:color="@color/color_gray" />                                 <!--for marks those are at right side of thumb-->
    //</selector>
    public Builder tickMarksColor(StateElement tickMarksColorStateList) {
        this.tickMarksColorStateList = tickMarksColorStateList;
        return this;
    }

    /**
     * set the seek bar's tick width , if tick type is divider, call this method will be not worked(tick type is divider,has a regular value 2dp).
     *
     * @param tickMarksSize the dp size.
     * @return Builder
     */
    public Builder tickMarksSize(int tickMarksSize) {
        this.tickMarksSize = SizeUtils.vp2px(context, tickMarksSize);
        return this;
    }

    /**
     * call this method to custom the tick showing drawable.
     *
     * @param tickMarksDrawable the drawable show as tickMark.
     * @return Builder
     */
    public Builder tickMarksDrawable(Element tickMarksDrawable) {
        this.tickMarksDrawable = tickMarksDrawable;
        return this;
    }

    /**
     * call this method to custom the tick showing drawable by selector.
     *
     * @param tickMarksStateListDrawable the StateListDrawable show as tickMark.
     * @return Builder
     * selector format like :
     */
    //<?xml version="1.0" encoding="utf-8"?>
    //<selector xmlns:ohos="http://schemas.ohos.com/apk/res/ohos">
    //<item ohos:drawable="@mipmap/ic_launcher_round" ohos:state_pressed="true" />  <!--this drawable is for thumb when pressing-->
    //<item ohos:drawable="@mipmap/ic_launcher" />  <!--for thumb when normal-->
    //</selector>
    public Builder tickMarksDrawable(StateElement tickMarksStateListDrawable) {
        this.tickMarksDrawable = tickMarksStateListDrawable;
        return this;
    }

    /**
     * call this method to hide the tickMarks which show in the both ends sides of seek bar.
     *
     * @param tickMarksEndsHide true for hide.
     * @return Builder
     */
    public Builder tickMarksEndsHide(boolean tickMarksEndsHide) {
        this.tickMarksEndsHide = tickMarksEndsHide;
        return this;
    }

    /**
     * call this method to hide the tickMarks on seekBar's thumb left;
     *
     * @param tickMarksSweptHide true for hide.
     * @return Builder
     */
    public Builder tickMarksSweptHide(boolean tickMarksSweptHide) {
        this.tickMarksSweptHide = tickMarksSweptHide;
        return this;
    }

}