# IndicatorSeekBar

## 项目介绍
- 项目名称：IndicatorSeekBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：这是一个openharmony自定义可滑动进度条库。
- 项目移植状态：主功能完成
- 调用差异：有，使用IndicatorSeekBar时需要在父布局添加一层DependentLayout
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 2.1.1


## 效果演示

**预览**

<img alt="Bar sample" src="gif/overview.png" width = "392" height = "115"/><br/>

<img alt="Bar sample" src="gif/continuous.gif" width = "264" height = "464"/>
<img alt="Bar sample" src="gif/discrete_1.gif" width = "264" height = "464"/><br/>

<img alt="Bar sample" src="gif/discrete_2.gif" width = "264" height = "464"/>
<img alt="Bar sample" src="gif/custom.gif" width = "264" height = "464"/><br/>

<img alt="Bar sample" src="gif/java_build.gif" width = "264" height = "464"/>
<img alt="Bar sample" src="gif/indicator.gif" width = "264" height = "464"/>
  

## 安装教程
1.在项目根目录下的build.gradle文件中，
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/release/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，
```
dependencies {
    implementation 'com.gitee.chinasoft_ohos:IndicatorSeekbar:1.0.0'   
}
```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
**xml**

```xml
<com.warkiz.widget.IndicatorSeekBar
    ohos:height="match_content"
    ohos:width="match_parent"
    app:isb_max="100"
    app:isb_min="-1.0"
    app:isb_progress="25"
    app:isb_seek_smoothly="true"
    app:isb_ticks_count="5"
    app:isb_show_tick_marks_type="1"
    app:isb_tick_marks_size="13vp"
    app:isb_tick_marks_drawable="$media:ic_launcher"
    app:isb_show_tick_texts="true"
    app:isb_tick_texts_size="15fp"
    app:isb_tick_texts_color="$color:color_blue"
    app:isb_thumb_color="$color:color_green"
    app:isb_thumb_size="20vp"
    app:isb_show_indicator="2"
    app:isb_indicator_color="$color:color_gray"
    app:isb_indicator_text_color="$color:colorAccent"
    app:isb_indicator_text_size="18fp"
    app:isb_track_background_color="$color:color_gray"
    app:isb_track_background_size="2vp"
    app:isb_track_progress_color="$color:color_blue"
    app:isb_track_progress_size="4vp" />
```

**Java**

```Java

 IndicatorSeekBar seekbar = IndicatorSeekBar
                .with(getContext())
                .max(110)
                .min(10)
                .progress(53)
                .tickCount(7)
                .showTickMarksType(TickMarkType.OVAL)
                .tickMarksColor(getColor(ResourceTable.Color_color_blue))
                .tickMarksSize(13)//vp
                .showTickTexts(true)
                .tickTextsColor(getColor(ResourceTable.Color_color_pink))
                .tickTextsSize(13)//fp
                .tickTextsTypeFace(Font.MONOSPACE)
                .showIndicatorType(IndicatorType.ROUNDED_RECTANGLE)
                .indicatorColor(getColor(ResourceTable.Color_color_blue))
                .indicatorTextColor(new Color(Color.getIntColor("#ffffff")))
                .indicatorTextSize(13)//fp
                .thumbColor(getColor(ResourceTable.Color_colorAccent))
                .thumbSize(14)
                .trackProgressColor(new Color(mContext.getColor(ResourceTable.Color_colorAccent)))
                .trackProgressSize(4)
                .trackBackgroundColor(new Color(mContext.getColor(ResourceTable.Color_color_gray)))
                .trackBackgroundSize(2)
                .build();

```

<big>**指示器总是停留**</big>
将IndicatorSeekBar放进IndicatorStayLayout后可以使Indicator总是停留，在此之前，请确保你使用属性去显示indicator.

**Xml**

```xml
<com.warkiz.widget.IndicatorStayLayout
    ohos:height="match_content"
    ohos:width="match_parent">
    <!--your layout-->
    <com.warkiz.widget.IndicatorSeekBar
        ohos:height="match_content"
        ohos:width="match_parent"
        app:isb_show_indicator="rectangle" <!--显示指示器属性不能为none-->
        ....../>
    <!--your layout-->
</com.warkiz.widget.IndicatorStayLayout>
```

**Java**

```Java
IndicatorSeekBar seekbar = IndicatorSeekBar
                .with(getContext())
                .max(50)
                .min(10)
                .showIndicatorType(IndicatorType.RECTANGLE) //显示指示器属性不能为none
                ...
                .build();

new IndicatorStayLayout(getContext()).attachTo(seekbar);
```

<big>**自定义指示器文字**</big>

向IndicatorSeekBar设置一个带有占位符的格式字符串,`${PROGRESS}` 或者 `${TICK_TEXT}`,指示器的文字就会改变.

例如:
如果你想显示带百分号后缀的指示器进度文字: `%` ，代码如下：

```Java
seekbar.setIndicatorTextFormat("${PROGRESS} %")
```

如果你想显示带前缀 `I am`的指示器tick text文字 ，代码如下：

```Java
seekbar.setIndicatorTextFormat("I am ${TICK_TEXT}")
```

<big>**自定义每一节的track块颜色**</big>
seekbar上每一块track的颜色都能被设置：

```Java
seekBar.customSectionTrackColor(new ColorCollector() {
    @Override
    public boolean collectSectionTrackColor(int[] colorIntArr) {
        //the length of colorIntArray equals section count
        colorIntArr[0] = getColor(ResourceTable.Color_color_blue);
        colorIntArr[1] = getColor(ResourceTable.Color_color_gray);
        colorIntArr[2] = Color.getIntColor("#FF4081");
        ...
        return true; //True if apply color , otherwise no change
    }
});
```

<big>**支持Selector类型的图片或颜色**</big>

你可以为滑块和tick标记设置StateElement, 而且，tick下面的文字也支持StateElement,使用的格式如下:

滑块图片 state-container:

```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <!--this drawable is for thumb when pressing-->
    <item ohos:element="$media:ic_launcher_round" ohos:state="component_state_pressed" />
    <!--for thumb in normal-->
    <item ohos:element="$media:ic_launcher" ohos:state="component_state_empty"/>
</state-container>
```

滑块颜色 state-container:

```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <!--this color is for thumb which is at pressing status-->
    <item ohos:element="$color:colorAccent" ohos:state="component_state_pressed" />
    <!--for thumb which is at normal status-->
    <item ohos:element="$color:color_blue" ohos:state="component_state_empty"/>
</state-container>
```

tick标记图片 state-container：

```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <!--this drawable is for tickMarks those are at left side of thumb-->
    <item ohos:element="$media:ic_launcher_round" ohos:state="component_state_selected" />
    <!--for tickMarks in normal-->
    <item ohos:element="$media:ic_launcher" ohos:state="component_state_empty"/>
</state-container>
```

tick标记颜色 state-container：

```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <!--this color is for marks those are at left side of thumb-->
    <item ohos:element="$color:colorAccent" ohos:state="component_state_selected" />
    <!--for marks those are at right side of thumb-->
    <item ohos:element="$color:color_gray" ohos:state="component_state_empty"/>
</state-container>
```

tick文字颜色 state-container：

```xml
<?xml version="1.0" encoding="utf-8"?>
<state-container xmlns:ohos="http://schemas.huawei.com/res/ohos">
    <!--this color is for texts those are at left side of thumb-->
    <item ohos:element="$color:colorAccent" ohos:state="component_state_selected" />
    <!--for tick text which is stopped under thumb -->
    <item ohos:element="$color:color_blue" ohos:state="component_state_hovered" />
    <!--for texts those are at right side of thumb-->
    <item ohos:element="$color:color_gray" ohos:state="component_state_empty"/>
</state-container>
```

<big>**监听器**</big>
```Java
seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams p) {
                Log.i(TAG, seekParams.seekBar);
                Log.i(TAG, seekParams.progress);
                Log.i(TAG, seekParams.progressFloat);
                Log.i(TAG, seekParams.fromUser);
                //when tick count > 0
                Log.i(TAG, seekParams.thumbPosition);
                Log.i(TAG, seekParams.tickText);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            }
        });
```


## 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

## 版本迭代
- 1.0.0

## 版权和许可信息  

	Copyright (C) 2017 zhuangguangquan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
